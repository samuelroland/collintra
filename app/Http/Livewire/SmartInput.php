<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SmartInput extends Component
{
    public $element;    //the element we want to modify
    public $field;  //name of the field in this element
    public $value;  //current value of the element

    public $rules = ['required'];

    public $edit = false;   //edit mode by default on false

    protected function getRules()
    {
        return ['value' => $this->rules];
    }

    public function render()
    {
        return view('livewire.smart-input');
    }

    public function save()
    {
        $this->validate();

        //        $this->element[$this->field] = $this->value;
        $this->element->title = $this->value;
        $this->element->save();

        $this->edit = false;
    }
}