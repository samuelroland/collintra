<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Article;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LearnTest extends TestCase
{
    use RefreshDatabase;
    protected $seed = true;

    public function test_learn_default_route_exists()
    {
        $response = $this->get('/learn');
        $response->assertRedirect(route('articles.show', ['article' => Article::first()->id]));
    }

    public function test_redirection_to_first_article()
    {
        $firstArticle = Article::all()->first();
        $this->get("/learn")->assertRedirect($firstArticle->path());
    }

    public function test_learn_default_route_redirects_to_create_an_article_with_message_when_no_article()
    {
        Article::truncate();
        $response = $this->get("/learn");

        $response->assertRedirect(route('articles.create'));
        $response->assertSessionHas('noArticleMessage');
    }

    public function test_article_edit_page_works()
    {
        Article::factory(5)->create();
        $article = Article::first();

        $response = $this->get('/learn/' . $article->id . "/edit");

        $response->assertStatus(200);
    }

    public function test_learn_page_exists()
    {
        $this->get(route('articles.show', ['article' => 1]))->assertStatus(200);
    }

    public function test_learn_page_support_article_not_found()
    {
        $response = $this->get(route('articles.show', ['article' => 1555000]));

        //TODO: $response->assertSee("This article doesn't exist or you don't have access to it.");
        $response->assertStatus(404);
    }
}