<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreditsTest extends TestCase
{
    use RefreshDatabase;
    protected $seed = true;

    public function test_credits_page_exists()
    {
        $response = $this->get(route('credits'));

        $response->assertStatus(200);
    }

    public function test_credits_page_has_a_link_the_main_pages()
    {
        //$response = $this->get(route('home'));
    }
}