<div class="max-w-6xl m-auto prose-sm md:prose-lg prose-slate prose-headings:font-semibold prose-h4:text-[1.1em] prose-h1:font-normal prose-p:!my-3 prose-blockquote:bg-blue-100 prose-blockquote:py-1 prose-blockquote:border-l prose-blockquote:border-blue-600 px-0 md:px-10 lg:px-24 overflow-hidden">
    <div class="items-center w-full md:mb-5 pt-2">

        <h1 class="not-prose flex-1 !my-0 mr-2">{{ $currentArticle->title }}</h1>

        <div class="not-prose text-gray-500 my-3 text-sm flex flex-wrap">
            <div class="mr-1">Last update: <span title="{{ $currentArticle->updated_at->format("d.m.Y H:i:s") }}">
                    {{ $currentArticle->updated_at->format("d.m.Y") }}
                </span>
            </div>

            <div class="mr-1">| Revisions: 8 </div>

            <div class="mr-1">| Contributors: 3 </div>
            <div class="mr-1 ">| <button class=" px-1 bg-blue-100 hover:bg-blue-300 rounded-md"><span class="md:hidden" @click="alert('Article edition is disabled on small screens.')">Edit this article</span> <a class="hidden md:inline" href="/learn/{{ $currentArticle->id }}/edit" disabled>Edit this article</a> </button></div>
        </div>
    </div>

    <div class="">{!! $currentArticle->body !!}</div>
</div>
