<div class="relative my-1 mt-3 md:mt-1 md:my-0 " x-data="{selected: -1}">
    <input class="w-full max-w-lg md:w-60" type="text" placeholder="Search..." wire:model="search" @keyup.down=" selected++" @keyup.up="selected--" @keyup.enter="window.location = @js($articles->pluck('path'))[selected]">

    @if(trim($search) != '')
    <div class="absolute top-0 bg-blue-50 p-1 rounded-md border-blue-400 border mt-8 space-y-1 z-50">
        @foreach ($articles as $article)
        <a href="{{ $article->path() }}" class="hover:no-underline ">
            <div class="border-blue-700 hover:bg-blue-300 px-1" :class="{'bg-blue-300': selected == {{ $loop->index }}}">
                <hr class="mx-3">
                <span class="leading-7 whitespace-nowrap font-bold block max-w-xs overflow-hidden text-ellipsis">{{ $article->title }}</span>
                {{-- <span class="whitespace-nowrap  leading-tight block text-slate-700 font-semi text-sm leading-1">{{ $article->pieceOf($search) }}</span> --}}
            </div>
        </a>
        @endforeach
        @if(count($articles) == 0 || $articles == null)
        <div class="text-sm italic text-slate-700">No article found with "{{ $search }}" sorry...</div>
        @endif

    </div>
    @endif
</div>
