<div x-data="{ editMode: @entangle('edit').defer, enterEditMode(){this.editMode = true, $nextTick(() => { $refs.input.focus() })} }">
    <span @dblclick="enterEditMode">
        <span x-show="!editMode" class="cursor-pointer px-1 rounded-sm hover:bg-blue-100"> {{ $value }}</span>
    </span>

    <input class="text-3xl" x-cloak wire:focusout="save" @keyup.esc="editMode = false" wire:keyup.enter="save" x-ref="input" x-show="editMode" type="text" wire:model.defer="value" value="{{ $value }}">

    @error('value')
    <div class="text-error my-1">{{ $message }}</div>
    @enderror
</div>
