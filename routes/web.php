<?php

use App\Models\Article;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

Route::get('/learn', function () {
    $firstArticle = Article::all()->first();
    if ($firstArticle != null) {
        return redirect($firstArticle->path());
    } else {
        return redirect(route('articles.create'))->with('noArticleMessage', 'There is no article for the moment, but you can create the first one !');
    }
})->name('learn.default');

Route::get('/learn/create', function () {
    return view('articles.create');
})->name('articles.create');

Route::get('/learn/{article}', function (Article $article) {
    $articles = Article::with('articles')->get();
    return view('learn', ['articles' => $articles, 'currentArticle' => $article]);
})->name('articles.show');

Route::get('/learn/{article}/edit', function (Article $article) {
    $articles = Article::with('articles')->get();
    return view('learn', ['edit' => true, 'articles' => $articles, 'currentArticle' => $article]);
})->name('articles.update');

Route::get('/credits', function () {
    return view('credits');
})->name('credits');

Route::get('/about', function () {
    return view('about');
})->name('about');